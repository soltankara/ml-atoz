# data preprocessing

# importing libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

# importing datasets
dataset = pd.read_csv('./Data.csv')
# [r,c] 
# : -> means all rows
# :-1 -> means all columns exept last one
X = dataset.iloc[:, :-1].values
# 3 -> means the column index 3
Y = dataset.iloc[:, 3].values

# missing values
from sklearn.preprocessing import Imputer
# we want to replace our missing values with mean values
imputer = Imputer(missing_values='NaN', strategy='mean', axis=0)
# 1:3 -> means columns between index 1 and index 3 [1,2] 
imputer = imputer.fit(X[:, 1:3])
X[:, 1:3] = imputer.transform(X[:, 1:3])

# Categorical Data
# they contain categories. like yes/no or germany/france/spain
# they do not have relational order

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelEncoder_X = LabelEncoder()
X[:,0] = labelEncoder_X.fit_transform(X[:, 0])

# they do not have relational order. 
# To have numeric values can cause to understanding of one value is greater than other.
# To this end, we will have 3 columns instead of one, and will use 1/0.
oneHotEncoder = OneHotEncoder(categorical_features=[0])
X = oneHotEncoder.fit_transform(X).toarray()

# same for other columns
labelEncoder_Y = LabelEncoder()
Y = labelEncoder_Y.fit_transform(Y)


# Splitting the dataset into the Trainig set and Test set

# deprecated
# from sklearn.cross_validation import rain_test_split

from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

# Feature Scaling








